Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: HSQLDB
Source: https://hsqldb.org/
Files-Excluded:
 *.jar
 *.zip
 */apidocs/*
 hsqldb/doc/apidocssqltool/jquery/*
 hsqldb/doc/apidocssqltool/member-search-index.js

Files: *
Copyright: The HSQL Development Group
License: BSD-3-clause-HSQLDB

Files:
 hsqldb/doc/verbatim/src/org/hsqldb/server/Servlet.java
 hsqldb/src/org/hsqldb/RowAVL.java
 hsqldb/src/org/hsqldb/RowAVLDisk.java
 hsqldb/src/org/hsqldb/util/DatabaseManager.java
 hsqldb/src/org/hsqldb/util/Grid.java
 hsqldb/src/org/hsqldb/util/DatabaseManagerCommon.java
 hsqldb/src/org/hsqldb/util/TransferCommon.java
 hsqldb/src/org/hsqldb/util/Tree.java
 hsqldb/src/org/hsqldb/util/Transfer.java
 hsqldb/src/org/hsqldb/util/SQLStatements.java
 hsqldb/src/org/hsqldb/util/CodeSwitcher.java
 hsqldb/src/org/hsqldb/util/TransferTable.java
 hsqldb/src/org/hsqldb/Like.java
 hsqldb/src/org/hsqldb/server/Servlet.java
 hsqldb/src/org/hsqldb/server/ServerConnection.java
 hsqldb/src/org/hsqldb/sample/FindFile.java
 hsqldb/src/org/hsqldb/index/NodeAVLDisk.java
 hsqldb/src/org/hsqldb/index/NodeAVL.java
 hsqldb/src/org/hsqldb/index/IndexAVLMemory.java
 hsqldb/src/org/hsqldb/index/IndexAVL.java
 hsqldb/src/org/hsqldb/lib/StringConverter.java
Copyright: The HSQL Development Group
           The Hypersonic SQL Group
License: BSD-3-clause-HSQLDB and BSD-3-clause-Hypersonic

Files: hsqldb/src/org/hsqldb/util/TableSorter.java
Copyright: Oracle and or its affiliates
License: BSD-3-clause-Oracle

Files: hsqldb/doc/apidocssqltool/script.js
       hsqldb/doc/apidocssqltool/search.js
Copyright: Oracle and or its affiliates
License: GPL-2-with-classpath-exception

Files: debian/*
Copyright: 2005-2007, Peter Eisentraut <petere@debian.org>
           2007-2013, Rene Engelhard <rene@debian.org>
           2013,      Niels Thykier <niels@thykier.net>
           2015-2025, Markus Koschany <apo@debian.org>
License: BSD-3-clause-HSQLDB

License: BSD-3-clause-HSQLDB
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 Neither the name of the HSQL Development Group nor the names of its
 contributors may be used to endorse or promote products derived from this
 software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL HSQL DEVELOPMENT GROUP, HSQLDB.ORG,
 OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause-Hypersonic
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 Neither the name of the Hypersonic SQL Group nor the names of its
 contributors may be used to endorse or promote products derived from this
 software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE HYPERSONIC SQL GROUP,
 OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 This software consists of voluntary contributions made by many individuals
 on behalf of the Hypersonic SQL Group.

License: BSD-3-clause-Oracle
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
 .
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 .
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 .
    - Neither the name of Oracle or the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.
 .
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
  IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
  PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Apache-2.0
 On Debian systems, the full text of the Apache license 2.0
 can be found in the file '/usr/share/common-licenses/Apache-2.0'

License: GPL-2-with-classpath-exception
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License 2 as published by
 the Free Software Foundation.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
 .
 "CLASSPATH" EXCEPTION TO THE GPL
 .
 Certain source files distributed by Oracle America and/or its affiliates are
 subject to the following clarification and special exception to the GPL, but
 only where Oracle has expressly included in the particular source file's header
 the words "Oracle designates this particular file as subject to the "Classpath"
 exception as provided by Oracle in the LICENSE file that accompanied this code."
 .
    Linking this library statically or dynamically with other modules is making
    a combined work based on this library.  Thus, the terms and conditions of
    the GNU General Public License cover the whole combination.
 .
    As a special exception, the copyright holders of this library give you
    permission to link this library with independent modules to produce an
    executable, regardless of the license terms of these independent modules,
    and to copy and distribute the resulting executable under terms of your
    choice, provided that you also meet, for each linked independent module,
    the terms and conditions of the license of that module.  An independent
    module is a module which is not derived from or based on this library.  If
    you modify this library, you may extend this exception to your version of
    the library, but you are not obligated to do so.  If you do not wish to do
    so, delete this exception statement from your version.

